<?php session_start();  ?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: Activity</title>
</head>
<body>
    <h1>Client-Server Communications</h1>
    <?php if (!isset($_SESSION['status']) || $_SESSION['status'] === FALSE || $_SESSION['status'] === NULL): ?>
        <h3>Login</h3>
        <form method="POST" action="./server.php">
            <input type="hidden" name="action" value="login"/>
            Email: <input type="email" name="email" required/>
            Password: <input type="password" name="password" required/>
            <button type="submit">Login</button>
        </form>
        <br/>
        <h3>Register</h3>
        <form method="POST" action="./server.php">
            <input type="hidden" name="action" value="register"/>
            Email: <input type="email" name="email" required/>
            Password: <input type="password" name="password" required/>
            <button type="submit">Register</button>
        </form>
    <?php else : ?>
        <form method="POST" action="./server.php">
            <input type="hidden" name="action" value="logout"/>
            <button type="submit">Logout</button>
        </form>
    <?php endif; ?>
    <!-- Display session message -->
    <?php if (isset($_SESSION['message'])): ?>
        <p><?php echo $_SESSION['message'] ?></p>
        <p><?php echo $_SESSION['status'] ?></p>
    <?php endif; ?>
    <!-- Display session data -->
    <h3>Session Data</h3>
    <?php if (isset($_SESSION['tasks'])): ?>
        <pre><?php print_r($_SESSION['tasks']) ?></pre>
    <?php endif; ?>
</body>
</html>