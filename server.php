<?php

session_start();

class User {
    // login credentials
    public $loginCreds = [
        'email' => 'johnsmith@gmail.com',
        'password' => '1234'
    ];

    // register function
    public function register($email, $password) {
        $credentials = [
            'email' => $email,
            'password' => $password
        ];
  
        $_SESSION['tasks'] = $credentials;
        $_SESSION['status'] = TRUE;
        $_SESSION['message'] = 'Registration successful';
    }

    // login function
    public function login($email, $password) {
        
        if ($email === $this->loginCreds['email'] && $password === $this->loginCreds['password']) {
            $_SESSION['status'] = TRUE;
            $_SESSION['tasks'] = $credentials;
            $_SESSION['message'] = 'Login successful';
        } else {
            $_SESSION['status'] = FALSE;
            $_SESSION['message'] = 'Login failed';
        }
    }

    // logout function
    public function logout() {
        session_destroy();
        $_SESSION['status'] = FALSE;
        $_SESSION['message'] = 'Logout successful';
    }
}

$userList = new User();

if ($_POST['action'] === 'login') {
    $userList->login($_POST['email'], $_POST['password']);
} else if ($_POST['action'] === 'logout') {
    $userList->logout();
} else if ($_POST['action'] === 'register') {
    $userList->register($_POST['email'], $_POST['password']);
}

header('Location: ./index.php');
           